import Vue from 'vue';
import MatchList from '@/components/MatchList';

describe('MatchList.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(MatchList);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('.hello h1').textContent)
      .to.equal('Welcome to Your Vue.js App');
  });
});
