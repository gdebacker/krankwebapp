import Vue from 'vue';
import Vuetify from 'vuetify';
import Router from 'vue-router';
import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import MatchList from '@/components/MatchList';
import UserProfile from '@/components/UserProfile';

Vue.use(Vuetify)
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/matches',
      name: 'matches',
      component: MatchList,
    },
    {
      path: '/profile',
      name: 'profile',
      component: UserProfile,
    },
  ],
});
